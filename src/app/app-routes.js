import Vue from "vue";
import VueRouter from "vue-router";
import Guard from "vue-router-multiguard";
import store from "@/app/app-states";
import { Middlewares } from "@/app/shared/services/app-router/middlewares";
import PageNotFound from "@/app/shared/views/PageNotFound";
import Auth from "@/app/auth/views/Auth";
import Dashboard from "@/app/dashboard/Dashboard";

import { dashboardRoutes } from "@/app/dashboard/dashboard-routes";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    redirect: store.getters.IS_LOGGED_IN ? "/dashboard" : "/auth"
  },
  {
    path: "/auth",
    name: "Auth",
    component: Auth,
    beforeEnter: Guard([Middlewares.unAuthRequired])
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard,
    children: dashboardRoutes,
    beforeEnter: Guard([Middlewares.authRequired])
  },
  {
    path: "/404",
    name: "Page404",
    component: PageNotFound
  },
  {
    path: "*",
    redirect: "/404"
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
