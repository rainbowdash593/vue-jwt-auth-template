class Api {
  SIGN_IN = "/profile/sign-in";
  FETCH_AUTH_USER = "/profile/authuser";
}
export const ApiConstants = new Api();
