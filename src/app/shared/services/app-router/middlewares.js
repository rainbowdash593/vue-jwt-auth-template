import store from "@/app/app-states";

export const Middlewares = {
  authRequired(to, from, next) {
    const route = store.getters.IS_LOGGED_IN ? undefined : "/";
    next(route);
  },
  unAuthRequired(to, from, next) {
    const route = !store.getters.IS_LOGGED_IN ? undefined : "/";
    next(route);
  }
};
