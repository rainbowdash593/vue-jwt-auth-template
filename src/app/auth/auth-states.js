import { HttpClient } from "@/app/shared/services/http-client";
import { ApiConstants } from "@/app/app-constants";

const state = {
  isLoading: false,
  token: localStorage.getItem("token"),
  isUserLoaded: false,
  authUser: {}
};

const getters = {
  IS_AUTH_LOADING: state => state.isLoading,
  AUTH_TOKEN: state => state.token,
  IS_LOGGED_IN: state => !!state.token,
  IS_USER_LOADED: state => state.isUserLoaded
};

const mutations = {
  SET_AUTH_LOADING: (state, isLoading) => (state.isLoading = isLoading),
  SET_AUTH_TOKEN: (state, token) => {
    state.token = token;
    token
      ? localStorage.setItem("token", token)
      : localStorage.removeItem("token");
  },
  SET_AUTH_USER: (state, user) => {
    state.authUser = user;
    state.isUserLoaded = true;
  },
  FORGET_AUTH_USER: state => {
    state.authUser = {};
    state.isUserLoaded = false;
  }
};

const actions = {
  SIGN_IN: (context, params) => {
    context.commit("SET_AUTH_LOADING", true);
    return new Promise((resolve, reject) => {
      HttpClient.post(ApiConstants.SIGN_IN, params)
        .then(({ data }) => {
          const { token } = data;
          context.commit("SET_AUTH_USER", data);
          context.commit("SET_AUTH_TOKEN", token);
          HttpClient.defaults.headers.common.Authorization = `Bearer ${token}`;
          resolve(data);
        })
        .catch(() => reject())
        .finally(() => context.commit("SET_AUTH_LOADING", false));
    });
  },
  LOGOUT: context => {
    return new Promise(resolve => {
      context.commit("FORGET_AUTH_USER");
      context.commit("SET_AUTH_TOKEN", undefined);
      resolve();
    });
  },
  FETCH_USER: context => {
    context.commit("SET_AUTH_LOADING", true);
    return new Promise((resolve, reject) => {
      HttpClient.get(ApiConstants.FETCH_AUTH_USER)
        .then(({ data }) => {
          context.commit("SET_AUTH_USER", data);
          resolve(data);
        })
        .catch(() => {
          context.commit("FORGET_AUTH_USER");
          context.commit("SET_AUTH_TOKEN", undefined);
          reject();
        })
        .finally(() => context.commit("SET_AUTH_LOADING", false));
    });
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
