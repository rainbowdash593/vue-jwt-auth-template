import { Middlewares } from "@/app/shared/services/app-router/middlewares";
import Guard from "vue-router-multiguard";
import Settings from "@/app/dashboard/settings/views/Settings";
import Profile from "@/app/dashboard/profile/views/Profile";

export const dashboardRoutes = [
  {
    path: "profile",
    name: "Profile",
    component: Profile,
    beforeEnter: Guard([Middlewares.authRequired])
  },
  {
    path: "settings",
    name: "Settings",
    component: Settings,
    beforeEnter: Guard([Middlewares.authRequired])
  }
];
