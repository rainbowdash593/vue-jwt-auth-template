import Vue from "vue";
import Vuex from "vuex";
import auth from "../app/auth/auth-states";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth
  }
});
