const ToastPlugin = {};

ToastPlugin.install = Vue => {
  Vue.prototype.$toast = {
    success(message, duration = 3000) {
      Vue.notify({
        group: "main",
        text: message,
        speed: 700,
        duration: duration,
        type: "success",
        data: {}
      });
    },
    error(message, duration = 3000) {
      Vue.notify({
        group: "main",
        text: message,
        speed: 700,
        duration: duration,
        type: "error",
        data: {}
      });
    },
    warning(message, duration = 3000) {
      Vue.notify({
        group: "main",
        text: message,
        speed: 700,
        duration: duration,
        type: "warn",
        data: {}
      });
    }
  };
};

export default ToastPlugin;
