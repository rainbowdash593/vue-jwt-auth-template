import Vue from "vue";
import App from "./app/App";
import router from "./app/app-routes";
import store from "./app/app-states";
import vuetify from "./plugins/vuetify";
import Notifications from "vue-notification";
import ToastPlugin from "@/plugins/toaster";
import "./styles/app.scss";

Vue.config.productionTip = false;

Vue.use(Notifications);
Vue.use(ToastPlugin);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
